using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MiddleManagement_TheOfficeSimulator.BaseComponents;

namespace MiddleManagement_TheOfficeSimulator.Characters;

public class TestRoger : BaseNpcObject
{
    public TestRoger(BaseGame game, GraphicsDeviceManager graphicsDeviceManager) : base(game, graphicsDeviceManager)
    {
        IsCollidable = true;
        Position = new Vector2((GraphicsDeviceManager.PreferredBackBufferWidth / 2) + 50,
            GraphicsDeviceManager.PreferredBackBufferHeight / 2);
        _startingPosition = Position;
        ObjectSpeed = BaseObjectSpeed;
        AnimationFrameCount = 0;
        BasicMovementAnimationFramesDict = new Dictionary<CardinalDirections, List<string>>()
        {
            {
                CardinalDirections.NORTH,
                new List<string>
                {
                    "Roger/roger_walk_up1",
                    "Roger/roger_walk_up2",
                    "Roger/roger_walk_up3",
                    "Roger/roger_walk_up4",
                    "Roger/roger_walk_up5",
                    "Roger/roger_walk_up6",
                }
            },
            {
                CardinalDirections.SOUTH,
                new List<string>
                {
                    "Roger/roger_walk_down1",
                    "Roger/roger_walk_down2",
                    "Roger/roger_walk_down3",
                    "Roger/roger_walk_down4",
                    "Roger/roger_walk_down5",
                    "Roger/roger_walk_down6",
                }
            },
            {
                CardinalDirections.EAST,
                new List<string>
                {
                    "Roger/roger_walk_right1",
                    "Roger/roger_walk_right2",
                    "Roger/roger_walk_right3",
                    "Roger/roger_walk_right4",
                    "Roger/roger_walk_right5",
                    "Roger/roger_walk_right6",
                }
            },
            {
                CardinalDirections.WEST,
                new List<string>
                {
                    "Roger/roger_walk_left1",
                    "Roger/roger_walk_left2",
                    "Roger/roger_walk_left3",
                    "Roger/roger_walk_left4",
                    "Roger/roger_walk_left5",
                    "Roger/roger_walk_left6",
                }
            },
        };
        TargetAnimationFramesPerSecond = 8.0f;
        
    }

    public override void Load(SpriteBatch spriteBatch)
    {
        SpriteBatch = spriteBatch;
        BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_down");
        BaseDialogBox = new BaseDialogBox(GameContext, spriteBatch);
        _debugCollisionTexture = new Texture2D(SpriteBatch.GraphicsDevice, 1, 1);
        _debugCollisionTexture.SetData<Color>(new Color[]{Color.White});
        InitializeCollider();
        InitializeRaycast();
        BaseRaycast.SetExtensionLength(5);
        BaseDialogBox.ConstructDialogBox(MiniDialogueAssetList);
        BaseRaycast._debugCollisionTexture = new Texture2D(SpriteBatch.GraphicsDevice, 1, 1);
        BaseRaycast._debugCollisionTexture.SetData<Color>(new Color[]{Color.White});
        base.Load(spriteBatch: spriteBatch);
    }
    
    public override void Update(GameTime gameTime)
    {
        FrameStartPosition = Position;
        if (!ShowDialog)
        {
            NpcWander(gameTime);
        }
        base.Update(gameTime);
    }

    public override void Draw(GameTime gameTime)
    {
        SpriteBatch.Begin();
        if (ShowDialog)
        {
            BaseDialogBox.ShowDialogBox(Position);
        }
        SpriteBatch.Draw(BaseTexture2D, Position, Color.White);
        SpriteBatch.Draw(_debugCollisionTexture, BaseCollider.CollisionBounds, Color.Ivory * 0.4f);
        SpriteBatch.Draw(BaseRaycast._debugCollisionTexture, BaseRaycast.CollisionBounds, Color.Red);
        SpriteBatch.End();
        base.Draw(gameTime);
    }
}