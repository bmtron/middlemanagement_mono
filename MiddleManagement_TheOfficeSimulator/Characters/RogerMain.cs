using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MiddleManagement_TheOfficeSimulator.BaseComponents;

namespace MiddleManagement_TheOfficeSimulator.Characters;

public class RogerMain : BaseGameObject
{
    public bool InteractionKeyState = false;
    public RogerMain(BaseGame game, GraphicsDeviceManager graphicsDeviceManager) : base(game, graphicsDeviceManager)
    {
        IsCollidable = true;
        Position = new Vector2(GraphicsDeviceManager.PreferredBackBufferWidth / 2,
            GraphicsDeviceManager.PreferredBackBufferHeight / 2);
        ObjectSpeed = BaseObjectSpeed;
        AnimationFrameCount = 0;
        BasicMovementAnimationFramesDict = new Dictionary<CardinalDirections, List<string>>()
        {
            {
                CardinalDirections.NORTH,
                new List<string>
                {
                    "Roger/roger_walk_up1",
                    "Roger/roger_walk_up2",
                    "Roger/roger_walk_up3",
                    "Roger/roger_walk_up4",
                    "Roger/roger_walk_up5",
                    "Roger/roger_walk_up6",
                }
            },
            {
                CardinalDirections.SOUTH,
                new List<string>
                {
                    "Roger/roger_walk_down1",
                    "Roger/roger_walk_down2",
                    "Roger/roger_walk_down3",
                    "Roger/roger_walk_down4",
                    "Roger/roger_walk_down5",
                    "Roger/roger_walk_down6",
                }
            },
            {
                CardinalDirections.EAST,
                new List<string>
                {
                    "Roger/roger_walk_right1",
                    "Roger/roger_walk_right2",
                    "Roger/roger_walk_right3",
                    "Roger/roger_walk_right4",
                    "Roger/roger_walk_right5",
                    "Roger/roger_walk_right6",
                }
            },
            {
                CardinalDirections.WEST,
                new List<string>
                {
                    "Roger/roger_walk_left1",
                    "Roger/roger_walk_left2",
                    "Roger/roger_walk_left3",
                    "Roger/roger_walk_left4",
                    "Roger/roger_walk_left5",
                    "Roger/roger_walk_left6",
                }
            },
        };
        TargetAnimationFramesPerSecond = 8.0f;
    }

    public override void Load(SpriteBatch spriteBatch)
    {
        SpriteBatch = spriteBatch;
        BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_down");
        InitializeCollider();
        InitializeRaycast();
        BaseRaycast.SetExtensionLength(5);
        BaseCollider._debugCollisionTexture = new Texture2D(SpriteBatch.GraphicsDevice, 1, 1);
        BaseCollider._debugCollisionTexture.SetData<Color>(new Color[]{Color.White});
        BaseRaycast._debugCollisionTexture = new Texture2D(SpriteBatch.GraphicsDevice, 1, 1);
        BaseRaycast._debugCollisionTexture.SetData<Color>(new Color[]{Color.White});
        base.Load(spriteBatch: spriteBatch);
    }
    
    public override void Update(GameTime gameTime)
    {
        FrameStartPosition = Position;
        HandleMovementInput(gameTime);
        base.Update(gameTime);
    }

    private void HandleMovementInput(GameTime gameTime)
    {
        var kstate = Keyboard.GetState();

        if (kstate.IsKeyDown(Keys.LeftShift))
        {
            Console.WriteLine("SPRINTING");
            var sprintSpeed = BaseObjectSpeed * 1.5f;
            ObjectSpeed = sprintSpeed;
        }

        if (kstate.IsKeyDown(Keys.W))
        { 
            Position.Y -= ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            DirectionFacing = CardinalDirections.NORTH;
        }
        
        if (kstate.IsKeyDown(Keys.S))
        {
            Position.Y += ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            DirectionFacing = CardinalDirections.SOUTH;
        }
        
        if (kstate.IsKeyDown(Keys.A))
        {
            Position.X -= ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            DirectionFacing = CardinalDirections.WEST;
        }

        if (kstate.IsKeyDown(Keys.D))
        {
            Position.X += ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            DirectionFacing = CardinalDirections.EAST;
        }
        LoadTextureBasedOnDirectionFacing();
        if (kstate.IsKeyDown(Keys.A) || kstate.IsKeyDown(Keys.D) || kstate.IsKeyDown(Keys.W) ||
            kstate.IsKeyDown(Keys.S))
        {
            Animate(ref BaseTexture2D, BasicMovementAnimationFramesDict[DirectionFacing], gameTime);
        }

        if (!kstate.IsKeyDown(Keys.LeftShift))
        {
            ObjectSpeed = BaseObjectSpeed;
        }

        if (kstate.IsKeyDown(Keys.Space) && !InteractionKeyState)
        {
            InteractionKeyState = true;
            if (BaseRaycast.IsCurrentlyColliding)
            {
                Console.WriteLine(BaseRaycast.Colliders.FirstOrDefault()!.Id);
                BaseRaycast.Colliders.FirstOrDefault()!.CanShowDialog = true;
                BaseRaycast.Colliders.FirstOrDefault()!.DirectionFacing = InvertDirectionFacing(DirectionFacing);
                BaseRaycast.Colliders.FirstOrDefault()!.LoadTextureBasedOnDirectionFacing();
            }
        }
        if (BaseRaycast.IsCurrentlyColliding && BaseRaycast.Colliders.FirstOrDefault()!.CanShowDialog)
        {
            BaseRaycast.Colliders.FirstOrDefault()!.ShowDialog = true;
        }
        else
        {
            if (BaseRaycast != null && BaseRaycast.Colliders.Count > 0)
            {
                BaseRaycast.Colliders.FirstOrDefault()!.ShowDialog = false;
            }
        }
        
        if (!kstate.IsKeyDown(Keys.Space))
        {
            InteractionKeyState = false;
        }
    }
    public override void Draw(GameTime gameTime)
    {
        SpriteBatch.Begin();

        SpriteBatch.Draw(BaseTexture2D, Position, Color.White);
        SpriteBatch.Draw(BaseCollider._debugCollisionTexture, BaseCollider.CollisionBounds, Color.Ivory * 0.4f);
        SpriteBatch.Draw(BaseRaycast._debugCollisionTexture, BaseRaycast.CollisionBounds, Color.Red);
        SpriteBatch.End();
        base.Draw(gameTime);
    }
    
}