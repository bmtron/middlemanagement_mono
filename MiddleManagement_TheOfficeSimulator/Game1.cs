﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MiddleManagement_TheOfficeSimulator.BaseComponents;
using MiddleManagement_TheOfficeSimulator.Characters;

namespace MiddleManagement_TheOfficeSimulator;

public class Game1 : BaseGame
{
    private GraphicsDeviceManager _graphics;
    private SpriteBatch _spriteBatch;
    private RogerMain _roger;
    private TestRoger _roger2;
    private TestRoger _roger3;
    private TestRoger _roger4;
    
    public Game1()
    {
        _graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;
    }

    protected override void Initialize()
    {
        // TODO: Add your initialization logic here
        objectsInScene = new List<BaseGameObject>();
        _roger = new RogerMain(this, _graphics);
        _roger2 = new TestRoger(this, _graphics);
        _roger3 = new TestRoger(this, _graphics);
        _roger3 = new TestRoger(this, _graphics);
        _roger4 = new TestRoger(this, _graphics);
        _roger2.Id = 200;
        _roger3.Id = 300;
        _roger4.Id = 400;
        // _roger4.Position.X -= 150;
        // _roger4.Position.Y -= 250;
        // _roger3.Position.X += 50;
        objectsInScene.Add(_roger2);
        objectsInScene.Add(_roger);
        // objectsInScene.Add(_roger3);
        // objectsInScene.Add(_roger4);
        
        base.Initialize();
    }

    protected override void LoadContent()
    {
        _spriteBatch = new SpriteBatch(GraphicsDevice);
        _roger.Load(_spriteBatch);
        _roger2.Load(_spriteBatch);
        // _roger3.Load(_spriteBatch);
        // _roger4.Load(_spriteBatch);
        base.LoadContent();
    }

    protected override void Update(GameTime gameTime)
    {
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
            Keyboard.GetState().IsKeyDown(Keys.Escape))
            Exit();
        _roger.Update(gameTime);
        _roger2.Update(gameTime);
        // _roger3.Update(gameTime);
        // _roger4.Update(gameTime);

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        GraphicsDevice.Clear(Color.CornflowerBlue);
        _roger.Draw(gameTime);
        _roger2.Draw(gameTime);
        // _roger3.Draw(gameTime);
        // _roger4.Draw(gameTime);

        base.Draw(gameTime);
    }

    
}