namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public enum CardinalDirections
{
    NORTH,
    SOUTH,
    EAST,
    WEST
}