using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseRaycast : BaseCollider
{
    public Point RayOrigin;
    public Point RaySize;
    public Rectangle RayObject;
    private int _rayExtensionLength;
    public BaseRaycast(BaseGameObject pinnedObject, List<BaseGameObject> objectsInProximity) : base(pinnedObject, objectsInProximity)
    {
        RayOrigin = pinnedObject.Position.ToPoint();
        RaySize = CalculateRaySizeBasedOnDirectionFacing();
    }

    public void ConstructRay()
    {
        RaySize = CalculateRaySizeBasedOnDirectionFacing();
        RayOrigin = CalculateRayOriginBasedOnDirectionFacing();
        RayObject = new Rectangle(RayOrigin, RaySize);
        CollisionBounds = RayObject;
    }

    public void SetExtensionLength(int extLen)
    {
        _rayExtensionLength = extLen;
    }
    
    private Point CalculateRaySizeBasedOnDirectionFacing()
    {
        switch (PinnedObject.DirectionFacing)
        {
            case CardinalDirections.EAST:
            case CardinalDirections.WEST:
                return new Point(PinnedObject.BaseCollider.CollisionBounds.Width / 2 + _rayExtensionLength, 1);
            case CardinalDirections.NORTH:
            case CardinalDirections.SOUTH:
                return new Point(1, PinnedObject.BaseCollider.CollisionBounds.Height / 2 + _rayExtensionLength);
            default:
                return new Point(1, 1);
        }
    }

    private Point CalculateRayOriginBasedOnDirectionFacing()
    {
        switch (PinnedObject.DirectionFacing)
        {
            case CardinalDirections.EAST:
                return PinnedObject.BaseCollider.CollisionBounds.Center;
            case CardinalDirections.WEST:
                return new Point(PinnedObject.BaseCollider.CollisionBounds.X - _rayExtensionLength, 
                    PinnedObject.BaseCollider.CollisionBounds.Y + PinnedObject.BaseCollider.CollisionBounds.Height / 2);
            case CardinalDirections.NORTH:
                return new Point(PinnedObject.BaseCollider.CollisionBounds.X + PinnedObject.BaseCollider.CollisionBounds.Width / 2,
                    PinnedObject.BaseCollider.CollisionBounds.Y - _rayExtensionLength);
            case CardinalDirections.SOUTH:
                return PinnedObject.BaseCollider.CollisionBounds.Center;
            default:
                return new Point(1, 1);
        }
        
    }
}