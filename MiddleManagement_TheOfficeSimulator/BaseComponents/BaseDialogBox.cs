using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rectangle = System.Drawing.Rectangle;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseDialogBox : DrawableGameComponent
{
    private Rectangle _dialogBounds;
    private List<Texture2D> _textures;
    private Game _gameContext;
    private SpriteBatch _spriteBatch;
    private SpriteFont _spriteFont;
    private Vector2 _dialogBoxAnchor;
    public BaseDialogBox(Game game, SpriteBatch spriteBatch) : base(game)
    {
        _gameContext = game;
        _spriteBatch = spriteBatch;
        _spriteFont = _gameContext.Content.Load<SpriteFont>("File");
        _textures = new List<Texture2D>();
    }

    public void ConstructDialogBox(List<string> dialogAssets)
    {
        foreach (var asset in dialogAssets)
        {
            _textures.Add(_gameContext.Content.Load<Texture2D>(asset));
        }
    }

    public void ShowDialogBox(Vector2 anchorPos)
    {
        _dialogBoxAnchor = anchorPos;
        _dialogBoxAnchor.Y -= 10;
        _dialogBoxAnchor.X -= 10;
        var cumulativeWidth = 0;

        for (int i = 0; i < _textures.Count; i++)
        {
            if (i == 0)
            {
                _spriteBatch.Draw(_textures[i], _dialogBoxAnchor, Color.White);
                cumulativeWidth += _textures[i].Width;
            }
            else
            {
                var nextPosition = new Vector2(cumulativeWidth + _dialogBoxAnchor.X, _dialogBoxAnchor.Y);
                _spriteBatch.Draw(_textures[i], nextPosition, Color.White);
                cumulativeWidth += _textures[i].Width;
            }
        }

        
        var fontPos = new Vector2(_dialogBoxAnchor.X + 35, _dialogBoxAnchor.Y + 4);
        _spriteBatch.DrawString(_spriteFont, "Hello!", fontPos, Color.Black);
    }
}