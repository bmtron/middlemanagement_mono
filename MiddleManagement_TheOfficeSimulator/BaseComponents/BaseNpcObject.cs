using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseNpcObject : BaseGameObject
{

    private double _framesToIdleAnimationChange = 0;
    private double _framesToRandomMovement = 0;
    private double _movementFrames = 12000;


    protected List<string> MiniDialogueAssetList;
    private double _tetherRange = 50.0f;
    protected Vector2 _startingPosition;
    
    public BaseNpcObject(BaseGame game, GraphicsDeviceManager graphicsDeviceManager) : base(game, graphicsDeviceManager)
    {
        MiniDialogueAssetList = new List<string>
        {
            "Dialog/dialog_thin_left",
            "Dialog/dialog_thin_center",
            "Dialog/dialog_thin_center",
            "Dialog/dialog_thin_right"
        };
    }
    
    protected void NpcWander(GameTime gameTime)
    {
        var rand = new Random();
        _framesToIdleAnimationChange--;
        _framesToRandomMovement--;
        if (_framesToIdleAnimationChange <= 0)
        {
            TurnRandomly();
            _framesToIdleAnimationChange = rand.Next(45, 100);
        }

        if (_framesToRandomMovement <= 0)
        {
            if (_movementFrames > 0 && FrameStepWithinTetherRange())
            {
                _movementFrames--;
                if (!BaseRaycast.IsCurrentlyColliding)
                {
                    MoveInDirectionFacing(gameTime);
                }
                else
                {
                    LoadTextureBasedOnDirectionFacing();
                }
            }
            else
            {
                _framesToRandomMovement = rand.Next(45, 600);
                _movementFrames = rand.Next(45, 90);
                LoadTextureBasedOnDirectionFacing();
            }
        }
    }

    private void TurnRandomly()
    {
        DirectionFacing = GenerateRandomCharacterTurn();
        LoadTextureBasedOnDirectionFacing();
    }

    private void MoveInDirectionFacing(GameTime gameTime)
    {
        
        switch (DirectionFacing)
        {
            case CardinalDirections.EAST:
                Position.X += ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                break;
            case CardinalDirections.WEST:
                Position.X -= ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                break;
            case CardinalDirections.NORTH:
                Position.Y -= ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                break;
            case CardinalDirections.SOUTH:
                Position.Y += ObjectSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                break;
            default:
                break;
        }

        Animate(ref BaseTexture2D, BasicMovementAnimationFramesDict[DirectionFacing], gameTime);
    }

    private CardinalDirections GenerateRandomCharacterTurn()
    {
        var rand = new Random();
        switch (rand.Next(4))
        {
            case 0:
                return CardinalDirections.EAST;
            case 1:
                return CardinalDirections.WEST;
            case 2:
                return CardinalDirections.NORTH;
            case 3:
                return CardinalDirections.SOUTH;
            default:
                return CardinalDirections.EAST;
        }
    }

    private bool FrameStepWithinTetherRange()
    {
        float pixelsPerStep = ObjectSpeed / 60F;
        if (DirectionFacing == CardinalDirections.EAST
            && Math.Abs(Position.X + pixelsPerStep - _startingPosition.X) >= _tetherRange)
        {
            return false;
        }

        if (DirectionFacing == CardinalDirections.WEST
            && Math.Abs(Position.X + pixelsPerStep - _startingPosition.X) >= _tetherRange)
        {
            return false;
        }

        if (DirectionFacing == CardinalDirections.NORTH
            && Math.Abs(Position.Y - pixelsPerStep - _startingPosition.Y) >= _tetherRange)
        {
            return false;
        }

        if (DirectionFacing == CardinalDirections.SOUTH
            && Math.Abs(Position.Y + pixelsPerStep - _startingPosition.Y) >= _tetherRange)
        {
            return false;
        }

        return true;
    }
    
    
}