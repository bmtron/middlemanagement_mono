using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public interface IAnimatable
{
    public void Animate(ref Texture2D textureToAnimate, List<string> movementSet, GameTime gameTime);
}