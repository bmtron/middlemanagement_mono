using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseGame : Game
{
    private List<BaseGameObject> _objectsInScene;
    public List<BaseGameObject> objectsInScene { 
        get => this._objectsInScene;
        set => this._objectsInScene = value != null ? value : throw new ArgumentNullException();
    }
}
