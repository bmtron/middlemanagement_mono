using System.Collections.Generic;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public interface IObjectProvider
{
    public List<BaseGameObject> objectsInScene
    {
        get;
        set;
    }
}