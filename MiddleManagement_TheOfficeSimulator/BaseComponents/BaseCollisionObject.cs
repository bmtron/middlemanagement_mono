using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseCollisionObject : DrawableGameComponent, ICollidable
{
    public BaseCollisionObject(Game game) : base(game)
    {
    }

    public virtual void DefineCollisionBounds()
    {
        throw new System.NotImplementedException();
    }

    public void UpdateCollisionBoxPosition()
    {
        throw new System.NotImplementedException();
    }

    public void CheckForCollision()
    {
        throw new System.NotImplementedException();
    }

    public void CheckForCollision(List<BaseGameObject> objectsInProximity)
    {
        throw new System.NotImplementedException();
    }
}