using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MiddleManagement_TheOfficeSimulator.Characters;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseGameObject : DrawableGameComponent, IAnimatable
{
    public long Id;
    protected BaseGame GameContext;
    protected SpriteBatch SpriteBatch;
    protected GraphicsDeviceManager GraphicsDeviceManager;
    public Texture2D BaseTexture2D;
    protected Texture2D _debugCollisionTexture;
    
    public Vector2 Position;

    public CardinalDirections DirectionFacing;
    protected Dictionary<CardinalDirections, List<string>> BasicMovementAnimationFramesDict;
    
    protected double TimeOfLastAnimationFrame;
    
    protected float ObjectSpeed;
    protected float TargetAnimationFramesPerSecond;
    protected int AnimationFrameCount = 0;
    
    protected Vector2 FrameStartPosition;
    protected float BaseObjectSpeed = 200f;
    
    public bool IsCollidable;

    public BaseDialogBox BaseDialogBox;
    public BaseCollider BaseCollider;
    public BaseRaycast BaseRaycast;

    public bool IsCollidingWithPlayerRaycast;

    public bool CanShowDialog;
    public bool ShowDialog;
    
    public BaseGameObject(BaseGame game, GraphicsDeviceManager graphicsDeviceManager) : base(game)
    {
        DirectionFacing = CardinalDirections.SOUTH;
        GraphicsDeviceManager = graphicsDeviceManager;
        GameContext = game;
    }

    public void InitializeCollider()
    {
        if (IsCollidable)
        {
            BaseCollider = new BaseCollider(this, GameContext.objectsInScene);
            BaseCollider.InitializeCollider(0.85, 0.7);
        }
    }

    public void InitializeRaycast()
    {
        BaseRaycast = new BaseRaycast(this, GameContext.objectsInScene);
        BaseRaycast.ConstructRay();
    }
    public virtual void Load(SpriteBatch spriteBatch){}

    public virtual void Animate(ref Texture2D textureToAnimate, List<string> movementSet, GameTime gameTime)
    {
        if (BaseCollider != null)
        {
            BaseCollider.UpdateCollisionRectPos(Position);
        }

        if (BaseRaycast != null)
        {
            BaseRaycast.ConstructRay();
        }
        if (TimeOfLastAnimationFrame == 0)
        {
            TimeOfLastAnimationFrame = gameTime.TotalGameTime.TotalSeconds;
        }
        
        if (AnimationFrameCount == movementSet.Count - 1)
        {
            AnimationFrameCount = 0;
        }
        textureToAnimate = GameContext.Content.Load<Texture2D>(movementSet[AnimationFrameCount]);

        if (gameTime.TotalGameTime.TotalSeconds - TimeOfLastAnimationFrame >=
            (1 / TargetAnimationFramesPerSecond))
        {
            TimeOfLastAnimationFrame = gameTime.TotalGameTime.TotalSeconds;
            AnimationFrameCount++;
        }
    }
    
    public override void Update(GameTime gameTime)
    {
        if (BaseCollider != null)
        {
            BaseCollider.CheckForCollision(GameContext.objectsInScene);
            if (BaseCollider.IsCurrentlyColliding)
            {
                CollideNSlide();
                BaseCollider.UpdateCollisionRectPos(Position);
                BaseRaycast?.ConstructRay();
            }
        }
        if (BaseRaycast != null)
        {
            BaseRaycast.ConstructRay();
            BaseRaycast.CheckForCollision(GameContext.objectsInScene);
        }
        base.Update(gameTime);
    }
    
    protected void CollideNSlide()
    {
        var tempRectXCheck = ConstructRectangle(FrameStartPosition.X, Position.Y, BaseTexture2D);
        var tempRectYCheck = ConstructRectangle(Position.X, FrameStartPosition.Y, BaseTexture2D);
        
        if (!GameContext.objectsInScene.Any(co => tempRectXCheck.Intersects(co.BaseCollider.CollisionBounds) && co != this))
        {
            Position.X = FrameStartPosition.X;
        }
        if (!GameContext.objectsInScene.Any(co => tempRectYCheck.Intersects(co.BaseCollider.CollisionBounds) && co != this))
        {
            Position.Y = FrameStartPosition.Y;
        }
        
        var finalRectCheck = ConstructRectangle(Position.X, Position.Y, BaseTexture2D);
        if (GameContext.objectsInScene.Any(co => finalRectCheck.Intersects(co.BaseCollider.CollisionBounds) && co != this))
        {
            Position = FrameStartPosition;
        }
    }

    private Rectangle ConstructRectangle(float xPos, float yPos, Texture2D baseTexture)
    {
        return new Rectangle((int)(xPos + (baseTexture.Width * (1 - BaseCollider.WidthScalar) / 2.0)),
            (int)(yPos + baseTexture.Height * (1 - BaseCollider.HeightScalar)),
            (int)(baseTexture.Width * BaseCollider.WidthScalar), (int)(baseTexture.Height * BaseCollider.HeightScalar));
    }
    public void LoadTextureBasedOnDirectionFacing()
    {
        switch (DirectionFacing)
        {
            case CardinalDirections.EAST:
                BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_right");
                break;
            case CardinalDirections.WEST:
                BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_left");
                break;
            case CardinalDirections.NORTH:
                BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_up");
                break;
            case CardinalDirections.SOUTH:
                BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_down");
                break;
            default:
                BaseTexture2D = GameContext.Content.Load<Texture2D>("Roger/Roger_still_down");
                break;
        }
    }
    

    public CardinalDirections InvertDirectionFacing(CardinalDirections directionFacing)
    {
        switch (directionFacing)
        {
            case CardinalDirections.EAST:
                return CardinalDirections.WEST;
            case CardinalDirections.WEST:
                return CardinalDirections.EAST;
            case CardinalDirections.NORTH:
                return CardinalDirections.SOUTH;
            case CardinalDirections.SOUTH:
                return CardinalDirections.NORTH;
            default:
                return CardinalDirections.SOUTH;
        }
    }
}