using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public interface ICollidable
{
    public void DefineCollisionBounds();
    public void UpdateCollisionBoxPosition();
    public void CheckForCollision();
    public void CheckForCollision(List<BaseGameObject> objectsInProximity);
    public void CheckForCollision(List<BaseGameObject> objectsInProximity, GameTime gameTime);
}