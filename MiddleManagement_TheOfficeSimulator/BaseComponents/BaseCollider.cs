using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MiddleManagement_TheOfficeSimulator.BaseComponents;

public class BaseCollider
{
    public BaseGameObject PinnedObject;
    public Rectangle CollisionBounds;
    public Texture2D BaseTexture;
    public double WidthScalar;
    public double HeightScalar;
    public List<BaseGameObject> ObjectsInProximity;
    public Texture2D _debugCollisionTexture;
    public bool IsCurrentlyColliding;
    public List<BaseGameObject> Colliders;
    public BaseCollider(BaseGameObject pinnedObject, List<BaseGameObject> objectsInProximity)
    {
        Colliders = new List<BaseGameObject>();
        PinnedObject = pinnedObject;
        ObjectsInProximity = objectsInProximity;
    }

    public void InitializeCollider(double widthScalar, double heightScalar)
    {
        WidthScalar = widthScalar;
        HeightScalar = heightScalar;
        BaseTexture = PinnedObject.BaseTexture2D;
        CollisionBounds = GenerateCollisionRect(PinnedObject.Position);
    }
    public void CheckForCollision(List<BaseGameObject> objectsInProximity)
    {
        if (objectsInProximity == null)
        {
            return;
        }
        foreach (var gameObject in objectsInProximity)
        {
            if (!gameObject.BaseCollider.CollisionBounds.IsEmpty && 
                gameObject.IsCollidable && 
                CollisionBounds.Intersects(gameObject.BaseCollider.CollisionBounds) && gameObject != PinnedObject)
            {
                Colliders.Add(gameObject);
                IsCurrentlyColliding = true;
                return;
            }
        }
        Colliders.ForEach(c =>
        {
            c.ShowDialog = false;
            c.CanShowDialog = false;
        });
        Colliders.Clear();
        IsCurrentlyColliding = false;
    }

    public Rectangle GenerateCollisionRect(Vector2 newPos)
    {
        return new Rectangle((int)(newPos.X + (BaseTexture.Width * (1 - WidthScalar) / 2.0)),
            (int)(newPos.Y + BaseTexture.Height * (1 - HeightScalar)),
            (int)(BaseTexture.Width * WidthScalar), (int)(BaseTexture.Height * HeightScalar));
    }

    public void UpdateCollisionRectPos(Vector2 newPos)
    {
        CollisionBounds = new Rectangle((int)(newPos.X + BaseTexture.Width * (1 - WidthScalar) / 2.0),
            (int)(newPos.Y + BaseTexture.Height * (1 - HeightScalar)),
            (int)(BaseTexture.Width * WidthScalar), (int)(BaseTexture.Height * HeightScalar));
    }
}